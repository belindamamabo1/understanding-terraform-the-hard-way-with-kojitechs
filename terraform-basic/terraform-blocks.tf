# this block is meant to st constraint on terraform version 
terraform {
    required_version = ">=1.1.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

## provider Block (Authentication and Authorization)
provider "aws" {
  profile = "default"
}

# Resource Block
# Create a VPC
resource "aws_vpc" "example" {
  cidr_block = var.cidr_block # cidr (re-usable) # variable
    tags = {
      name = "terraform-testVpc"  
}
}

resource "aws_subnet" "main" {
  vpc_id     = aws_vpc.example.id
  cidr_block = "10.0.1.0/24" # var.variable
  #parameter.......>
  availability_zone = "us-east-1a" #data.source
}




# Creating ec2 inatance
          # local_name    # resource_name (private ip)
resource "aws_instance" "ec2_instance" {
  ami           = var.ami_id # us-east-1
  instance_type = "t2.micro"
    tags = {
      name = "ec2_instance"
  }
}
# VARIABLE BLOCK
#### variable (type of variables...) # string
#"" # double sttring
## descriptive in naming variables 
### creating a variables 

variable "ami_id" {
  type = string
  description = "ec2 instance ami id"
  default = "ami-0b0dcb5067f052a63"
}

variable "cidr_block" {
  type = string
  description = "vpc-cidr"
  default = "10.0.0.0/16"
  
}

# How do I reference the variable (var.ami_id)


#### OUTPUT BLOCK
# terraform console (  aws_instance.ec2_instance)
output "private_dns" {
  description = "Private dns id"
  value = "aws_instance.ec2_instance.private_dns"
}

output "primary_network_interface_id" {
  description = "primary_network_interface_id"
  value = "aws_instance.ec2_instance.primary_network_interface_id"
}

# Data source block
# use to pull down already created resources from aws
# use a resource using data source # -target
data "aws_availability_zones" "available" {
  state = "available"
}